import java.util.Iterator;
import java.util.List;

import static java.util.Collections.EMPTY_LIST;

class PartitionStops {
  public static List<List<Stop>> run(List<Stop> stops) {
    List<List<Stop>> stopsOfStops = List.of(stops);

    final Iterator<Stop> stopIterator = stops.iterator();

    while (stopIterator.hasNext()) {
      final Stop stop = stopIterator.next();
      final int indexOfStop = stops.indexOf(stop);

      if (stop.isMaj()
          &&  indexOfStop != 0
          && indexOfStop != stops.size() - 1) {
        stops.add(indexOfStop, stop);
      }
    }

    return stopsOfStops;
  }

  private static boolean isMaj(String stop) {
    return stop.equals("MAJ");
  }
}
